package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Controller
public class IndexController {

    private final VisitsRepository visitsRepository;

    public IndexController(VisitsRepository visitsRepository) {
        this.visitsRepository = visitsRepository;
    }

    @GetMapping("/")
    public ModelAndView index() {
        Map<String, String> model = new HashMap<>();
        model.put("name", "Akim");

        Visit visit = new Visit();
        visit.description = String.format("Visited at %s", LocalDateTime.now());
        visitsRepository.save(visit);
        System.out.println("test"); // @TODO remove this line

        return new ModelAndView("index", model);
    }

    /*
     * Binding a request parameter to a method parameter using 'name'
     * attribute of @RequestParam
     */
    @RequestMapping("/addVisit")
    @ResponseBody
    public String namesHandler(@RequestParam(name = "name") String name) {
        Visit visit = new Visit();
        visit.description = String.format("%s visited at %s", name, LocalDateTime.now());
        visitsRepository.save(visit);

        return String.format("URL parameter <br>  name = %s ", name);
    }
}
